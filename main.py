from dataclasses import field
from email.mime import image
from multiprocessing.connection import wait
import matplotlib.pyplot as plt
import json
from PIL import Image, ImageDraw
import matplotlib.image as mpimg
import cv2
import matplotlib.pyplot as plt
from shapely.geometry import Polygon
from os import listdir
from os.path import isfile, join
import os
from pathlib import Path

videos = []
frames = []
image_dir = "Images"
pred_dir = "Predictions"
predictions = {}

def calculate_iou(box_1, box_2):
    poly_1 = Polygon(box_1)
    poly_2 = Polygon(box_2)
    iou = poly_1.intersection(poly_2).area / poly_1.union(poly_2).area
    return iou



def bbox_coordinates(x1,y1,x2,y2):
    coordinates = [[x1,y1],[x2,y1],[x2,y2],[x1,y2]]
    return coordinates

def read_predictions():
    for i in range(len(frames)):
        img_loc = os.path.join(image_dir,(videos[i]+frames[i]+".png"))
        im = Image.open(img_loc)
        pred_loc = os.path.join(pred_dir,(img_loc.split("\\")[1] + ".json"))
        print(pred_loc)
        if(pred_loc.split("\\")[1]) in prediction_files:
            dictionary = json.load(open(pred_loc, 'r'))
            height = dictionary["image_height"]
            width = dictionary["image_width"]
            bbox = dictionary["pred_boxes"]
            keypoints = dictionary["keypoints"]
            #print("height:", height)
            #print("width:", width)
            #print("pred_boxes:", bbox)
            #print("keypoints:", keypoints)
            if(len(keypoints[0])!=4):
                continue
            #print("located")
            im = im.resize((width,height))
            draw = ImageDraw.Draw(im) 
            colors = ["red", "green", "blue", "yellow","purple", "orange"]
            draw.rectangle(bbox[0], fill='red')
            draw.polygon(
                        [keypoints[0][0][0],keypoints[0][0][1],
                        keypoints[0][1][0],keypoints[0][1][1],
                        keypoints[0][3][0],keypoints[0][3][1],
                        keypoints[0][2][0],keypoints[0][2][1],
                        keypoints[0][0][0],keypoints[0][0][1]], 
                        fill='yellow') 
            kps = [
                    [keypoints[0][0][0],keypoints[0][0][1]],
                    [keypoints[0][1][0],keypoints[0][1][1]],
                    [keypoints[0][3][0],keypoints[0][3][1]],
                    [keypoints[0][2][0],keypoints[0][2][1]]
                    ]

            box = bbox_coordinates(bbox[0][0], bbox[0][1], bbox[0][2], bbox[0][3])
            print(box)
            print(kps)
            iou=calculate_iou(box,kps)*100
            print("IOU: " + str(round(iou,5)) + "%")
            predictions[pred_loc.split("\\")[1]] = iou
            print()
            #im.show()
        else:
            print("no prediction found for file " + img_loc)
            continue




def image_files (dir):
    images =   os.listdir(dir)
    f = "f"
    for file in images:
        video, rame = file.split("f")
        f = "f" 
        frame = f+rame
        videos.append(video)
        frames.append(frame.split(".")[0])




def convert_preds_to_json (dir):
    for file in (os.listdir(dir)): 
        file = Path(os.path.join(dir,file))
        file.rename(file.with_suffix('.json'))
    



image_files(image_dir)
convert_preds_to_json(pred_dir)   
prediction_files = os.listdir(pred_dir) 
read_predictions()
print(predictions)
with open('IOU.json', 'w') as fp:
    json.dump(predictions, fp)

 






dictionary = json.load(open("Predictions\\video_1641200145526frames000016.png.json", 'r'))
height = dictionary["image_height"]
width = dictionary["image_width"]
bbox = dictionary["pred_boxes"]
keypoints = dictionary["keypoints"]
im = Image.open("Images\\video_1641200145526frames000016.png")
draw = ImageDraw.Draw(im) 
draw.rectangle(bbox[0], fill='red')
draw.polygon(
            [keypoints[0][0][0],keypoints[0][0][1],
            keypoints[0][1][0],keypoints[0][1][1],
            keypoints[0][3][0],keypoints[0][3][1],
            keypoints[0][2][0],keypoints[0][2][1],
            keypoints[0][0][0],keypoints[0][0][1]], 
            fill='yellow') 
im.show()








